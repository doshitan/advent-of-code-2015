import           Day1

import           Prelude    (IO, Maybe (..), ($))
import           Test.Hspec

main :: IO ()
main = hspec $ do
  describe "part1" $
    it "returns correct value for official test input" $
      part1 officialTestInput `shouldBe` 74
  describe "part2" $
    it "returns correct value for official test input" $
      part2 officialTestInput `shouldBe` (Just 1795,7000,74)
