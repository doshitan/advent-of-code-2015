{-# LANGUAGE OverloadedStrings #-}
import           Day1

import           Criterion      (Benchmark, bench, bgroup, nf)
import           Criterion.Main (defaultMain)
import           Data.Text      (Text, replicate, unpack)
import           Prelude        (IO)

largeInput :: Text
largeInput = replicate 1000000 "("

main :: IO ()
main = defaultMain
  [ bgroup "Day1 official input" (benchmarks officialTestInput)
  , bgroup "Day1 large input" (benchmarks largeInput)
  ]

benchmarks :: Text -> [Benchmark]
benchmarks input =
  [ bench "part1" (nf part1 input)
  , bench "part1 with string" (nf part1 (unpack input))
  , bench "part1Alt" (nf part1Alt input)
  , bench "part2" (nf part2 input)
  ]
