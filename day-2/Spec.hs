{-# LANGUAGE OverloadedStrings #-}
import           Day2

import           Prelude    (IO, ($))
import           Test.Hspec

main :: IO ()
main = hspec $ do
  describe "part1" $ do
    it "returns correct value for example input 1" $
      part1 "2x3x4" `shouldBe` 58
    it "returns correct value for example input 2" $
      part1 "1x1x10" `shouldBe` 43
    it "returns correct value for official input" $
      part1 officialTestInput `shouldBe` 1606483
  describe "part2" $ do
    it "returns correct value for example input 1" $
      part2 "2x3x4" `shouldBe` 34
    it "returns correct value for example input 2" $
      part2 "1x1x10" `shouldBe` 14
    it "returns correct value for official input" $
      part2 officialTestInput `shouldBe` 3842356
