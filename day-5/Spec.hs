{-# LANGUAGE OverloadedStrings #-}
import           Day5

import           Prelude    (IO, ($))
import           Test.Hspec

main :: IO ()
main = hspec $ do
  describe "judgeWord" $ do
    it "returns Nice when all conditions are met" $
      judgeWord "ugknbfddgicrmopn" `shouldBe` Nice
    it "returns Nice when all conditions are met with overlapping letters" $
      judgeWord "aaa" `shouldBe` Nice
    it "returns Naughty when no double letter" $
      judgeWord "jchzalrnumimnmhp" `shouldBe` Naughty
    it "returns Naughty when contains 'xy'" $
      judgeWord "haegwjzuvuyypxyu" `shouldBe` Naughty
    it "returns Naughty when only contains one vowel" $
      judgeWord "dvszwmarrgswjxmb" `shouldBe` Naughty
  describe "part1" $
   it "returns correct value for official input" $
     part1 officialTestInput `shouldBe` 255
  describe "judgeWord2" $ do
    it "returns Nice when all conditions are met" $
      judgeWord2 "qjhvhtzxzqqjkmpb" `shouldBe` Nice
    it "returns Nice when all conditions are met with overlapping letters" $
      judgeWord2 "xxyxx" `shouldBe` Nice
    it "returns Nice for 'aaaa'" $
      judgeWord2 "aaaa" `shouldBe` Nice
    it "returns Naughty when no single letter repeat with single letter between" $
      judgeWord2 "uurcxstgmygtbstg" `shouldBe` Naughty
    it "returns Naughty when no pair appears twice" $
      judgeWord2 "ieodomkazucvgmuy" `shouldBe` Naughty
  describe "part2" $
    it "returns correct value for official input" $
      part2 officialTestInput `shouldBe` 55
