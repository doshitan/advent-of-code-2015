{-# LANGUAGE OverloadedStrings #-}
import           Day3

import           Prelude    (IO, ($))
import           Test.Hspec

main :: IO ()
main = hspec $ do
  describe "part1" $ do
    it "returns correct value for example input 1" $
      part1 ">" `shouldBe` 2
    it "returns correct value for example input 2" $
      part1 "^>v<" `shouldBe` 4
    it "returns correct value for example input 3" $
      part1 "^v^v^v^v^v" `shouldBe` 2
    it "returns correct value for official input" $
      part1 officialTestInput `shouldBe` 2565
  describe "part2" $ do
    it "returns correct value for example input 1" $
      part2 "^v" `shouldBe` 3
    it "returns correct value for example input 2" $
      part2 "^>v<" `shouldBe` 3
    it "returns correct value for example input 3" $
      part2 "^v^v^v^v^v" `shouldBe` 11
    it "returns correct value for official input" $
      part2 officialTestInput `shouldBe` 2639
