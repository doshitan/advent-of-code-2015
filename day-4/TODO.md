# TODOs
* Implement a version that uses the bitwise operations instead of utilizing the
  BitVector and benchmark it
* Implement a version that converts to string and looks at the characters
  instead of utilizing the BitVector and benchmark it
* Implement parallel version and benchmark it
