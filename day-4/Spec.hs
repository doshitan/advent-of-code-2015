{-# LANGUAGE OverloadedStrings #-}
import           Day4

import           Prelude    (IO, ($))
import           Test.Hspec

main :: IO ()
main = hspec $ do
  describe "part1" $ do
    it "returns correct value for example input 1" $
      part1 "abcdef" `shouldBe` 609043
    it "returns correct value for example input 2" $
      part1 "pqrstuv" `shouldBe` 1048970
    it "returns correct value for official input" $
      part1 officialTestInput `shouldBe` 117946
  describe "part2" $
    it "returns correct value for official input" $
      part2 officialTestInput `shouldBe` 3938038
