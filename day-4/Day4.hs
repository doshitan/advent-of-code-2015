{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Day4 where

import           ClassyPrelude  hiding (hash, unpack)
import           Crypto.Hash
import           Data.Bits
import           Data.ByteArray (unpack)
import           Data.Text      (strip)
import qualified Data.Vector    as V

day4Main :: IO ()
day4Main = do
  input <- strip <$> hGetContents stdin
  putStrLn $ "Number needed to get 5 zeros at beginning of hash: " <> tshow (part1 input)
  putStrLn $ "Number needed to get 6 zeros at beginning of hash: " <> tshow (part2 input)

md5 :: ByteString -> Digest MD5
md5 = hash

allInts :: [Int]
allInts = [1..]

-- great idea from https://stackoverflow.com/a/7301278
type BitVector = V.Vector Bool

unpackBitVector :: (FiniteBits a) => a -> BitVector
unpackBitVector w = V.generate (finiteBitSize w) (testBit w)

packBitVector :: (Num a, Bits a) => BitVector -> a
packBitVector v = V.ifoldl' set 0 v
  where
    set w i True = w `setBit` i
    set w _ _    = w

firstXNibblesAreZero :: Int -> Digest MD5 -> Bool
firstXNibblesAreZero num hash' = (V.all (== False) . take (num * 4) . concatMap (reverse . unpackBitVector) . unpack) hash'

generateHashes :: Text -> [(Digest MD5, Int)]
generateHashes input = map (\i -> (md5 . encodeUtf8 . (++) input . tshow $ i, i)) allInts

partCommon :: Int -> Text -> Int
partCommon numNibbles input = foldr stepState 0 (generateHashes input)
  where
    stepState :: (Digest MD5, Int) -> Int -> Int
    stepState (hash', val) res = if firstXNibblesAreZero numNibbles hash' then val else res

part1 :: Text -> Int
part1 = partCommon 5

part2 :: Text -> Int
part2 = partCommon 6

officialTestInput :: Text
officialTestInput = "ckczppom"
