import           Day4

import           Criterion      (Benchmark, bench, bgroup, nf)
import           Criterion.Main (defaultMain)
import           Data.Text      (Text)
import           Prelude        (IO)

main :: IO ()
main = defaultMain
  [ bgroup "Day4 official input" (benchmarks officialTestInput)
  ]

benchmarks :: Text -> [Benchmark]
benchmarks input =
  [ bench "part1" (nf part1 input)
  , bench "part2" (nf part2 input)
  ]
